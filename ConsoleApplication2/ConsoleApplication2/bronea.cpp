#include "bronea.h"
bronea::bronea(int ar_cl, int cons=0, int speed=0, int lovkosti=0, int sila=0, int popad=0, int uron=0) {
	this->ar_cl = ar_cl;
	this->cons = cons;
	this->speed = speed;
	this->lovkosti = lovkosti;
	this->sila = sila;
	this->popad = popad;
	this->uron = uron;
};
vector<bronea> fill_bronea() {
	bronea none(10);
	bronea sheet(9);
	bronea koja(8);
	bronea koj_shhet(7);
	bronea kolitsa(6);
	bronea cheshuia(5);
	bronea kshp(4);
	bronea plast(3);
	bronea pl_sheet(2);
	bronea pants(1);
	bronea poln_plants(0);

	vector<bronea>vbronea = { none, sheet, koja, koj_shhet, kolitsa, cheshuia, kshp, plast, pl_sheet, pants, poln_plants };
	return vbronea;
};